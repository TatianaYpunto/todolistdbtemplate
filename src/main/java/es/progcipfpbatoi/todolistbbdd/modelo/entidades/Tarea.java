package es.progcipfpbatoi.todolistbbdd.modelo.entidades;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

public class Tarea {

    private int codigo;

    private String usuario;

    private String descripcion;

    private LocalDateTime fechaCreacion;
    
    private LocalDate fechaVencimiento;
    
    private LocalTime horaVencimiento;
    
    private Prioridad prioridad;

    private boolean realizada;

    public Tarea(int codigo) {
       this.codigo = codigo;
    }
    
    public Tarea(int codigo, String usuario, String descripcion) {
         this(codigo, usuario, descripcion, Prioridad.MEDIA, LocalDate.now().plusDays(2), LocalTime.now(), false);
    }

    public Tarea(int codigo, String usuario, String descripcion, LocalDate fechaVencimiento, LocalTime horaVencimiento) {
        this(codigo, usuario, descripcion, Prioridad.MEDIA, fechaVencimiento, horaVencimiento, false);
    }

    public Tarea(int codigo, String usuario, String descripcion, Prioridad priority, LocalDate fechaVencimiento, LocalTime horaVencimiento,boolean realizado) {
        this(codigo, usuario, descripcion, LocalDateTime.now(), priority,  fechaVencimiento, horaVencimiento, realizado);
    }

    public Tarea(int codigo, String usuario, String descripcion, LocalDateTime creadoEn, Prioridad priority, LocalDate fechaVencimiento, LocalTime horaVencimiento, boolean realizado) {
        set(codigo, usuario, descripcion, creadoEn, priority, fechaVencimiento, horaVencimiento, realizado);
    }
    
    public void set(Tarea tarea) {
    	set(tarea.codigo, tarea.usuario, tarea.descripcion, tarea.fechaCreacion, tarea.prioridad, tarea.fechaVencimiento, tarea.horaVencimiento, tarea.realizada);
	}
    
    private void set(int codigo, String usuario, String descripcion, LocalDateTime creadoEn, Prioridad priority, LocalDate fechaVencimiento, LocalTime horaVencimiento, boolean realizado) {
    	this.codigo = codigo;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.prioridad = priority;
        this.fechaCreacion = creadoEn;
        this.fechaVencimiento = fechaVencimiento;
        this.horaVencimiento = horaVencimiento;
        this.realizada = realizado;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDateTime getCreadoEn() {
        return fechaCreacion;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public LocalDateTime getVencimiento() {
        return LocalDateTime.of(fechaVencimiento, horaVencimiento);
    }

    public int getCodigo() {
        return codigo;
    }

    public boolean isRealizado() {
        return realizada;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tarea)) {
            return false;
        }
        Tarea tarea = (Tarea) o;
        return getCodigo() == tarea.getCodigo();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }

	public boolean perteneceAUsuario(String usuario) {
		return this.usuario.equals(usuario);
	}

	
}
